const {schemaObjectValidator} = require('./schema-object-validator');
const expect = require('expect.js');

describe('validates objects by means of a given schema', function() {
    const carSchema = {
        brand: 'string',
        type: 'string',
        milage: 'number',
        extras: 'array',
        autopilot: 'boolean',
        owner: 'object'
    };

    it('should validate to true', function () {
        const carObj = {
            brand: 'Mazda',
            type: 'MX5 NB 1.8',
            milage: 199999.99,
            extras: [
                '2001 Special Edition'
            ],
            autopilot: false,
            owner: {
                firstName: 'Marcel',
                lastname: 'Beitelbeck'
            }
        };

        const validated = schemaObjectValidator(carSchema, carObj);
        expect(validated).to.be(true);
    });

    it('has a mismatching property', function () {
        const carObjF = {
            brand: 'BMW',
            type: '335',
            milage: "103021",
            extras: [
                'LCI',
                'KW Coilovers',
            ],
            autopilot: false,
            owner: {
                firstName: 'Marcel',
                lastname: 'Beitelbeck'
            }
        };

        const validated = schemaObjectValidator(carSchema, carObjF);
        expect(validated).to.be(false);
    });

    it('has too few properties', function () {
        const carObjF = {
            brand: 'BMW',
            type: '335',
            milage: 103021,
            extras: [
                'LCI',
                'KW Coilovers',
            ],
            autopilot: false
        };

        const validated = schemaObjectValidator(carSchema, carObjF);
        expect(validated).to.be(false);
    });
});





