// check that an object is not of type array
function isObject(obj) {
    return typeof obj === "object" && !Array.isArray(obj)
}

/**
 * Makes sure that an given object matches its schema
 *
 * @param schema
 * @param object
 * @returns {boolean}
 */
const schemaObjectValidator = (schema, object) => {
    const schemaEntries = Object.entries(schema);

    // loop over the given schema and check that every property matches its type
    // went everything as planned the valid is untouched and returns true
    // is there a mismatch its been set to false and stops the loop
    // are there too few arguments its type is undefined which doesn't matches and evaluates to false
    let valid = true;
    for (const [key, value] of schemaEntries) {
        if (value === "array") {
            if (!Array.isArray(object[key])) {
                valid = false;
                break;
            }
        } else if (value === "object") {
            if (!isObject(object[key])) {
                valid = false;
                break;
            }
        } else {
            if (typeof object[key] !== value) {
                valid = false;
                break;
            }
        }
    }
    return valid;
};

exports.schemaObjectValidator = schemaObjectValidator;
